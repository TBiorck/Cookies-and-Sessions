package se.experis.noticeboard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboard.util.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class MainController {

    @Autowired
    private SessionController sessionController;

    @GetMapping("/notice/add")
    public String newNotice(@CookieValue(value = "username", defaultValue = "") String username,
                            HttpSession session) {

        if(!username.isEmpty() && sessionController.isAuthorized(session)){
            return "new-notice";
        }
        else {
            return "redirect:/login";
        }
    }

    @GetMapping("/notice/{id}")
    public String getNotice(@PathVariable("id") Integer id) {
        return "notice";
    }

    @GetMapping(value = "/login")
    public String login(@CookieValue(value = "foreverCookie", defaultValue = "") String foreverCookie,
                        HttpServletRequest request,
                        HttpSession session) {
        return "login";
    }

    @GetMapping(value = "/register")
    public String login(HttpServletRequest request,
                        HttpSession session) {
        return "register";
    }

}
