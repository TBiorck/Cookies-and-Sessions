package se.experis.noticeboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboard.models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Integer> {
    UserModel getByUsername(String username);
    UserModel getById(String id);
    boolean existsUserModelByUsername(String username);
}
