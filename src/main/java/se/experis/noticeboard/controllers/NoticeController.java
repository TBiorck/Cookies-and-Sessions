package se.experis.noticeboard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboard.models.Comment;
import se.experis.noticeboard.models.CommonResponse;
import se.experis.noticeboard.models.Notice;
import se.experis.noticeboard.repository.NoticeRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.util.Collections;
import java.util.List;

@RestController("/api")
public class NoticeController {

    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private SessionController sessionController;

    @GetMapping("/api/notice")
    public ResponseEntity<CommonResponse> getNotices(HttpServletRequest request) {

        // Get and sort all notices by most recent first
        List<Notice> notices = noticeRepository.findAll();
        Collections.sort(notices, Collections.reverseOrder());

        CommonResponse cr = new CommonResponse();
        cr.data = notices;
        cr.message = "Returning all notices";

        System.out.println("Returning all notices");

        HttpStatus response = HttpStatus.OK;

        return new ResponseEntity<>(cr, response);
    }

    @GetMapping("/api/notice/{id}")
    public ResponseEntity<CommonResponse> getNoticeById(HttpServletRequest request, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Get notice if it exists
        if (noticeRepository.existsById(id)) {
            cr.data = noticeRepository.getById(id);
            cr.message = "Notice with id: " + id;
            response = HttpStatus.OK;
        }
        else {
            cr.data = null;
            cr.message = "Did not find notice with id: " + id;
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<CommonResponse>(cr, response);
    }


    @PostMapping("/api/notice")
    public ResponseEntity<CommonResponse> addNotice(@CookieValue(value = "username", defaultValue = "") String username,
                                                    HttpSession session,
                                                    @RequestBody Notice notice) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Return default message if unauthorized
        if(unauthorized(username, session)) {
            return unauthorizedResponse(cr);
        }

        // Username is set by cookie which is not part of the RequestBody
        notice.author = username;

        // Add notice if it is valid
        if(isValidNotice(notice)) {
            notice = noticeRepository.save(notice);

            System.out.println("New notice with id: " + notice.id);
            response = HttpStatus.CREATED;
            cr.data  = notice;
            cr.message = "New notice wth id: " + notice.id;

        } else {
            cr.message = "Invalid notice inputs.";
            response = HttpStatus.NO_CONTENT;
        }

        return new ResponseEntity<>(cr, response);
    }

    @PostMapping("/api/notice/{id}")
    public ResponseEntity<CommonResponse> addComment(@CookieValue(value = "username", defaultValue = "") String username,
                                                     HttpSession session,
                                                     @PathVariable("id") Integer noticeId,
                                                     @RequestBody Comment comment) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Return default message if unauthorized
        if(unauthorized(username, session)) {
            return unauthorizedResponse(cr);
        }

        // Add comment if the notice it should be commented on exists
        if (noticeRepository.existsById(noticeId)) {
            // Create an SQL date out of a util.Date
            Date date = new Date(new java.util.Date().getTime());
            comment.date = date;

            Notice notice = noticeRepository.getById(noticeId);
            notice.addComment(comment);
            noticeRepository.save(notice);

            // Id of comment is set when added to the Notice. Get its id from last index of comment list
            Integer generatedId = notice.getComments().get(notice.getComments().size() -1).id;

            cr.data = comment;
            cr.message = "Added comment with id " + generatedId;
            System.out.println("Added comment with id " + generatedId);
            response = HttpStatus.CREATED;
        }
        else {
            cr.message = "Did not find notice with id: " + noticeId;
            System.out.println("Did not find notice with id: " + noticeId);
            response = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(cr, response);
    }

    @DeleteMapping("/api/notice/{id}")
    public ResponseEntity<CommonResponse> deleteNotice(@CookieValue(value = "username", defaultValue = "") String username,
                                                       HttpSession session,
                                                       @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Return default message if unauthorized
        if(unauthorized(username, session)) {
            return unauthorizedResponse(cr);
        }

        // Delete notice if it exists
        if (noticeRepository.existsById(id)) {
            noticeRepository.deleteById(id);
            cr.message = "Deleted notice with id: " + id;
            System.out.println("Deleted notice with id: " + id);
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Did not find notice with id: " + id;
            System.out.println("Did not find notice with id: " + id);
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    @PatchMapping("/api/notice/{id}")
    public ResponseEntity<CommonResponse> updateNotice(@CookieValue(value = "username", defaultValue = "") String username,
                                                       HttpSession session,
                                                       @RequestBody Notice newNotice,
                                                       @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Return default message if unauthorized
        if(unauthorized(username, session)) {
            return unauthorizedResponse(cr);
        }

        // Update if notice exists
        if (noticeRepository.existsById(id)) {
            Notice notice = noticeRepository.getById(id);
            if (isValidNotice(newNotice)) {
                notice.title = newNotice.title;
                notice.text = newNotice.text;
                notice.date = newNotice.date;
            }
            noticeRepository.save(notice);

            cr.data = notice;
            cr.message = "Updated notice with id: " + notice.id;
            System.out.println("Updated notice with id: " + notice.id);
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Did not find notice wih id: " + id;
            System.out.println("Did not find notice wih id: " + id);
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    /* HELPER METHODS */

    private boolean isValidNotice(Notice notice) {
        return notice != null && notice.text != null && notice.title != null;
    }

    private boolean unauthorized(String username, HttpSession session) {
        return username.isEmpty() || !sessionController.isAuthorized(session);
    }

    private ResponseEntity<CommonResponse> unauthorizedResponse(CommonResponse cr) {
        System.out.println("Unauthorized");
        cr.message = "Unauthorized";
        return new ResponseEntity<>(cr, HttpStatus.UNAUTHORIZED);
    }
}
