package se.experis.noticeboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboard.models.Notice;

public interface NoticeRepository extends JpaRepository<Notice, Integer> {
    Notice getById(Integer id);
}
