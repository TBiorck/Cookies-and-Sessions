/**
 * This file contains the functions for the HTML page login.html.
 */



/**
 * Uses api.js to do a request on the api for logging in a user.
 */
async function login() {
    resetAlerts()

    const username = document.getElementById('inputUsername').value
    const password = document.getElementById('inputPassword').value

    const data = { username: username, password: password }
    const response = await saveSession(data)

    const status = response.status
    let json = await response.json()

    showLoginStatus(status, json.message)
}

/**
 *
 * @param {number} status
 * @param {string} message
 */
function showLoginStatus(status, message) {
    showAlert(status, message)

    if (status == 200) {
        hideLoginForm()
    }
}

/**
 * Hide the login form if login success.
 */
function hideLoginForm() {
    document.getElementById('login-container').hidden = true
}