/**
 * This file contains the functions for the HTML page register.html.
 */


/**
 * Uses api.js to do a request on the api for creating a new user.
 */
async function doRegister() {
    resetAlerts()

    const username = document.getElementById('inputUsername').value
    const password = document.getElementById('inputPassword').value

    const data = { username: username, password: password }
    const response = await registerUser(data)

    const status = response.status
    let json = await response.json()

    showRegisterStatus(status, json.message)
}

/**
 *
 * @param {number} status
 * @param {string} message
 */
function showRegisterStatus(status, message) {
    showAlert(status, message)

    if (status == 200) {
        hideRegisterForm()
    }
}

/**
 * Hide the register form if registration succeeds.
 */
function hideRegisterForm() {
    document.getElementById('register-container').hidden = true
}