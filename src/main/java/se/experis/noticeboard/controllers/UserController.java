package se.experis.noticeboard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboard.models.CommonResponse;
import se.experis.noticeboard.models.UserModel;
import se.experis.noticeboard.repository.UserRepository;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserRepository repository;

    @PostMapping("/user")
    public ResponseEntity<CommonResponse> registerUser(@RequestBody UserModel userModel) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if (repository.existsUserModelByUsername(userModel.username)) {
            cr.message = "Username is already taken";
            System.out.println("Username is already taken");
            
            response = HttpStatus.BAD_REQUEST;
        }
        else {
            userModel = repository.save(userModel);

            cr.data = userModel;
            cr.message = "Created user " + userModel.username + " with id: " + userModel.id;
            System.out.println("Created user " + userModel.username + " with id: " + userModel.id);

            response = HttpStatus.CREATED;
        }

        return new ResponseEntity<>(cr, response);
    }

    @GetMapping("/user")
    public ResponseEntity<CommonResponse> getUsers() {
        CommonResponse cr = new CommonResponse();
        HttpStatus response = HttpStatus.OK;

        List<UserModel> users = repository.findAll();

        cr.data = users;

        cr.message = "Get all users";

        return new ResponseEntity<>(cr, response);
    }

    @GetMapping("/user/{username}")
    public ResponseEntity<CommonResponse> getUserByUsername(@PathVariable("username") String username) {
        CommonResponse cr = new CommonResponse();

        UserModel user = repository.getByUsername(username);
        System.out.println("FOUND: " + user.username);

        cr.data = user;
        cr.message = "Found user";

        return new ResponseEntity<>(cr, HttpStatus.OK);
    }
}
