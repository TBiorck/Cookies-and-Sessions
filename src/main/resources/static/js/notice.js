const URL = window.location.pathname

/**
 * Fetches data using api.js and then render the fetched data on the html.
 */
async function loadNoticeData() {
    const id = URL.substring(URL.lastIndexOf('/') + 1)

    const response = await getNoticeById(id)


    const json = await response.json()

    addDataToHtml(json.data, true)
}

/**
 *
 * @param data
 * @param {boolean} alsoAddComments
 */
function addDataToHtml(data, alsoAddComments) {
    const title = document.getElementById('title')
    const inputTitle = document.getElementById('inputTitle')
    const text = document.getElementById('text')
    const date = document.getElementById('date')
    const author = document.getElementById('notice-author')

    const currentUser = getCookie("username")

    // If there is a logged in user
    if(currentUser) {
        allowCommenting(true)
    }
    else {
        allowCommenting(false)
    }

    // Remove buttons for edit and delete if not the author
    const buttonContainer = document.getElementById('buttons-container')
    if (userIsAuthor(currentUser, data.author)) {
        buttonContainer.hidden = false
    }
    else {
        buttonContainer.remove()
    }

    title.innerText = 'Title: ' + data.title
    inputTitle.value = data.title
    text.value = data.text
    date.value = data.date
    author.innerText = 'Author: ' + data.author

    if (alsoAddComments) {
        addComments(data.comments)
    }
}

/**
 * Add comments into a table.
 * @param {Array} comments
 */
function addComments(comments) {
    const commentsDiv = document.getElementById('comments')

    comments.forEach(comment => {
        appendNewCommentToHtml(comment)
    })
}

// Used for dividing comments
function createHorizontalLine() {
    let hr = document.createElement('hr')
    hr.classList.add('mt-2', 'mb-3')
    return hr
}


/* EDIT AND POST FUNCTIONS */

async function confirmUpdate() {
    const id = URL.substring(URL.lastIndexOf('/') + 1)

    const title = document.getElementById('inputTitle').value
    const text = document.getElementById('text').value
    const date = document.getElementById('date').value

    // Do patch with data
    const data = { title: title, text: text, date: date}
    const response = await updateNotice(data, id)

    const json = await response.json()

    // Dont reload comments on notice update
    addDataToHtml(json.data, false)

    disableEditNotice()
}

function enableEditNotice() {
    let inputFields = document.getElementsByTagName('input')
    let inputTitle = document.getElementById('inputTitle')
    let text = document.getElementById('text')

    // Enable input fields
    for (let field of inputFields) {
        field.disabled = false
    }
    text.disabled = false
    inputTitle.hidden = false

    // Show only confirm button
    document.getElementById('deleteButton').hidden = true
    document.getElementById('editButton').hidden = true
    document.getElementById('confirmButton').hidden = false
}

function disableEditNotice() {
    let inputFields = document.getElementsByTagName('input')
    let inputTitle = document.getElementById('inputTitle')
    let text = document.getElementById('text')

    // Disable input fields
    for (let field of inputFields) {
        field.disabled = true
    }
    text.disabled = true
    inputTitle.hidden = true

    // Show edit and delete button only
    document.getElementById('deleteButton').hidden = false
    document.getElementById('editButton').hidden = false
    document.getElementById('confirmButton').hidden = true
}

/**
 *
 * @param {boolean} allow
 */
function allowCommenting(allow) {
    const commentContainer = document.getElementById('new-comment-container')
    if (allow) {
        commentContainer.hidden = false
    }
    else {
        commentContainer.remove()
    }
}

async function removeNotice() {
    const id = URL.substring(URL.lastIndexOf('/') + 1)

    let response = await deleteNotice(id)

    const status = response.status
    let json = await response.json()
    const message = json.message

    document.getElementById('notice-info-container').remove()
    document.getElementById('comment-section').remove()
    showAlert(status, message)
}

async function postComment() {
    const id = URL.substring(URL.lastIndexOf('/') + 1)

    const text = document.getElementById('commentText').value
    const author = getCookie("username")
    //const today = new Date()

    // const date = today.getFullYear() + ':' + today.getMonth() + ':' + today.getDay()
    // console.log(date)

    const data = { text: text, author: author }

    const response = await addComment(data, id)
    const status = response.status

    const json = await response.json()


    document.getElementById('commentText').value = ''
    showAlert(status, json.message)
    appendNewCommentToHtml(json.data)
}

/**
 *
 * @param {JSON} comment
 */
function appendNewCommentToHtml(comment) {
    const commentContainer = document.createElement('div')

    const author = document.createElement('h5')
    const text = document.createElement('p')
    const date = document.createElement('p')

    author.innerText = 'Author: ' + comment.author
    text.innerText = comment.text
    date.innerText = comment.date

    commentContainer.appendChild(author)
    commentContainer.appendChild(text)
    commentContainer.appendChild(date)
    commentContainer.appendChild(createHorizontalLine())

    document.getElementById('comments').appendChild(commentContainer)
}

/**
 * Checks if the author of the notice is the same as the current user.
 * @param {string} currentUser
 * @param {string} noticeAuthor
 * @returns {boolean}
 */
function userIsAuthor(currentUser, noticeAuthor) {
    return currentUser === noticeAuthor
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}