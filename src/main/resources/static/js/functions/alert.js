/**
 * This file contains the functions for handling alerts.
 * It is used by HTML files:
 * login.html, new-notice.html, notice.html, register.html.
 */

/**
 * Show success or error alert depending on status code, along with a message.
 * @param {number} status
 * @param {string} message
 */
function showAlert(status, message) {
    let alertText
    if (status >= 200 && status < 300) {
        alertText = document.getElementById('success-text')
        alertText.innerText = message
        document.getElementById('success-alert').hidden = false
    }
    else {
        alertText = document.getElementById('error-text')
        alertText.innerText = message
        document.getElementById('error-alert').hidden = false
    }
}

// Alerts are set to their default state 'hidden'
function resetAlerts() {
    document.getElementById('error-alert').hidden = true
    document.getElementById('success-alert').hidden = true
}