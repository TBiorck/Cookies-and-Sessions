/**
 * This class is responsible for setting user cookies, and saving sessions using SessionKeeper.
 * It is also used to verify authority.
 */

package se.experis.noticeboard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboard.models.CommonResponse;
import se.experis.noticeboard.models.UserModel;
import se.experis.noticeboard.repository.UserRepository;
import se.experis.noticeboard.util.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RestController
public class SessionController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/saveSession")
    public ResponseEntity<CommonResponse> add(
            @RequestBody UserModel inputUser,
            HttpServletResponse response,
            HttpSession session) {

        CommonResponse cr = new CommonResponse();
        HttpStatus status;

        // Check if user exist in database
        if (userRepository.existsUserModelByUsername(inputUser.username)) {
            UserModel user = userRepository.getByUsername(inputUser.username);

            // Verify correct credentials
            if (inputUser.password.equals(user.password)) {
                System.out.println("Logged in successfully");
                SessionKeeper.getInstance().AddSession(session.getId());

                cr.message = "Logged in successfully";
                status = HttpStatus.OK;

                // Store username of current user
                Cookie currentUser = new Cookie("username", user.username);
                currentUser.setMaxAge(600); // in seconds. (10 min)
                response.addCookie(currentUser);
            }
            else {
                System.out.println("Login failed");
                cr.message = "Login failed";
                status = HttpStatus.UNAUTHORIZED;
            }
        }
        else {
            cr.message = "Wrong username or password!";
            status = HttpStatus.UNAUTHORIZED;
        }

        return new ResponseEntity<>(cr, status);
    }

    @GetMapping("/logout")
    public ResponseEntity<CommonResponse> removeSession(@CookieValue("username") String username,
                                                        HttpSession session,
                                                        HttpServletResponse response) {
        SessionKeeper.getInstance().RemoveSession(session.getId());

        Cookie currentUser = new Cookie("username", "");
        currentUser.setMaxAge(0);
        response.addCookie(currentUser);

        CommonResponse cr = new CommonResponse();
        cr.message = "Successfully logged out.";

        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

    public boolean isAuthorized(HttpSession session) {
        return SessionKeeper.getInstance().CheckSession(session.getId());
    }
}
