/**
 * This file contains the functions for the HTML page index.html.
 *
 */


/**
 * Fetches data using api.js and then render the fetched data on the html.
 */
async function loadNotices() {
    const response = await getNotices()

    const json = await response.json()

    addNoticesToHtmlTable(json.data)

    setWelcomeText()
}

/**
 * Iterates all notices and adds them to a HTML table.
 * @param {object[]} notices
 */
function addNoticesToHtmlTable(notices) {
    let tbody = document.getElementById('notice-table-body')

    let row = 1
    notices.forEach(notice => {
        let tr = document.createElement('tr')

        let thRow = document.createElement('th')
        thRow.setAttribute('scope', 'row')

        let tdDate = document.createElement('td')
        let tdAuthor = document.createElement('td')

        // Add title as link element
        let tdTitle = document.createElement('td')
        let a = document.createElement('a')
        a.setAttribute('href', '/notice/' + notice.id)    // Set href to this notice's endpoint

        // Set text of elements
        a.innerText = notice.title
        tdTitle.appendChild(a)

        thRow.innerText = '' + row++
        tdDate.innerText = notice.date
        tdAuthor.innerText = notice.author

        tr.appendChild(thRow)
        tr.appendChild(tdTitle)
        tr.appendChild(tdAuthor)
        tr.appendChild(tdDate)

        tbody.appendChild(tr)
    })
}

/**
 * Set the welcome text to either greet the user (if logged in) by name or just a default message.
 */
function setWelcomeText() {
    const welcome = document.getElementById('welcome-text')

    const username = getCookie("username")

    if (username) {
        welcome.innerText = 'Welcome ' + username + '!'
    }
    else {
        welcome.innerText = 'Welcome!'
    }

}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}