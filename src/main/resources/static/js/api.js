/**
 * This file contains the functions for creating api requests NoticeAPI, UserAPI and SessionAPI.
 * It is used by every html page.
 */

const NOTICE_API = '/api/notice'
const SESSION_API = '/saveSession'
const USER_API = '/user'


/* NOTICE AND COMMENT */

/**
 * Fetches all notices from database.
 * @returns {Promise<Response>}
 */
async function getNotices() {
    const response = await fetch(NOTICE_API)

    return response
}

/**
 * Fetch a specific notice from the database based on its id.
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function getNoticeById(id) {
    const response = await fetch(NOTICE_API + '/' + id)

    return response
}

/**
 * Create a notice with the input data
 * @param {Object} data
 * @returns {Promise<Response>}
 */
async function addNotice(data) {
    const response = await fetch(NOTICE_API, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}

/**
 * Create a comment with the input data
 * @param {Object} data
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function addComment(data, id) {
    const response = await fetch(NOTICE_API + '/' + id, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}

/**
 * Sends a delete request to the API for the input id.
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function deleteNotice(id) {
    const response = await fetch(NOTICE_API + '/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })

    return response
}

/**
 * Update the notice with the input data.
 * @param {object} data
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function updateNotice(data, id) {
    const response = await fetch(NOTICE_API + '/' + id, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}


/* SESSION */

/**
 * Create a notice with the input data
 * @param {Object} data
 * @returns {Promise<Response>}
 */
async function saveSession(data) {
    const response = await fetch(SESSION_API, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}


/* USER */

/**
 * Create a user with the input data
 * @param {Object} data
 * @returns {Promise<Response>}
 */
async function registerUser(data) {
    const response = await fetch(USER_API, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}
