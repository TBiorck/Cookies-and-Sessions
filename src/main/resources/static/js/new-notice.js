/**
 * This file contains the functions for the HTML page new-notice.html.
 */


/**
 * Uses api.js to do a request on the api for posting a new notice.
 */
async function submitNotice() {
    resetAlerts()

    // READ VALUES
    let title = document.getElementById('inputTitle').value
    let text = document.getElementById('inputText').value
    let date = document.getElementById('inputDate').value


    if (!title || !text || !date) {
        showAlert(null, 'Invalid or empty input! Try again.')
    }

    let data = {title: title, text: text, date: date}
    let response = await addNotice(data)

    const status = response.status
    let json = await response.json()
    const message = json.message

    showAlert(status, message)
}

