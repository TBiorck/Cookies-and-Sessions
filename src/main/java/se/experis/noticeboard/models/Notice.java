package se.experis.noticeboard.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Notice implements Comparable<Notice> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String title;

    @Column(length = 5000)
    public String text;

    @Column
    public Date date;

    @Column
    public String author;

    @Override
    public int compareTo(Notice notice) {
        return date.compareTo(notice.getDate());
    }

    @OneToMany(mappedBy="notice", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    public void addComment(Comment comment) {
        comments.add(comment);
        comment.setNotice(this);
    }

    public void removeComment(Comment comment) {
        comments.remove(comment);
        comment.setNotice(null);
    }

    public Date getDate() {
        return date;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
